package main

import "fmt"

func f(from string) {
	for i := 0; i < 3; i++ {
		fmt.Println(from, ":", i)
	}
}

func main() {
	f("direct")

	go f("goroutine")

	go func(msg string) {
		fmt.Println(msg)
	}("going")

	go func() {
		go f("another goroutine")
		go f("another goroutine1")
		go f("another goroutine2")
	}()

	fmt.Scanln()
	fmt.Println("done")
}